const productController = require('./../controllers/product.ctrl')

module.exports = (router) => {

    /**
     * get all products
     */
    router
        .route('/products')
        .post(productController.getAll)

    /**
     * add an product
     */
    router
        .route('/product')
        .post(productController.addProduct)

}