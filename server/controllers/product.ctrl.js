const Product = require('./../models/Product')

module.exports = {
    addProduct: (req, res, next) => {
        let productObj = req.body
        console.log(req.body)
        new Product(productObj).save((err, product) => {
            if (err)
                res.send(err)
            else if (!product)
                res.send(400)
            else {
                res.send(product)
            }
            next()
        })
    },
    getAll: (req, res, next) => {
        console.log("test")
        Product.find({}, function(err, products){
            if (err)
                res.send(err)
            else if (!products)
                res.send(404)
            else {
                let return_data = {
                    "searchSuccess": true,
                    "dataTotalSize": 10,
                    "data": products
                }
                res.send(return_data)
            }
            next()
        });
    },
}