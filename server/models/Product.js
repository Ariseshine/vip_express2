const mongoose = require('mongoose')

let ProductSchema = new mongoose.Schema(
    {
        category: String,
        brand: String,
        model: String,
        color: String,
        quantity: Number
    }
);

module.exports = mongoose.model('Product', ProductSchema)