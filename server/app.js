const express = require("express")
const morgan  = require('morgan')

const mongoose = require('mongoose')
const url = process.env.MONGODB_URI || "mongodb://localhost:27017/honyan"

const cors = require('cors')
const bodyParser = require('body-parser')
const helmet = require('helmet')

const app = express()
const router = express.Router()
const routes = require('./routes/')

/** connect to MongoDB datastore */
try {
    mongoose.connect(url, {
        //useMongoClient: true
    })
} catch (error) {

}

/** set up middlewares */
app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
// app.use(helmet())
const env = process.env.NODE_ENV || 'dev'
if(env==="dev"){
    app.use(morgan('combined')); // log every request to the console
}

/** set up routes {API Endpoints} */
app.use('/', express.static('build'))
routes(router)
app.use('/api', router)

/** start server */
let port = 5000 || process.env.PORT
app.listen(port, () => {
    console.log(`Server started at port: ${port}`);
});