import React, { Component } from 'react';

import translationsJSON from '../translations/translations.json';
import LocalizedStrings from 'react-localization';
import {
    ResourceList,
    TextStyle,
    Thumbnail,
    ActionList,
    AppProvider,
    Card,
    ContextualSaveBar,
    DisplayText,
    Form,
    FormLayout,
    Frame,
    Label,
    Layout,
    List,
    Loading,
    Modal,
    Navigation,
    Page,
    TextContainer,
    TextField,
    Toast,
    TopBar,
    SkeletonPage,
    SkeletonBodyText,
    SkeletonDisplayText,
    InlineError
} from '@shopify/polaris';


let translations = new LocalizedStrings(translationsJSON);
class CustomerPickupRequestCreate extends Component {
    defaultState = {
        emailFieldValue: 'dharma@jadedpixel.com',
        nameFieldValue: 'Jaded Pixel',
    };

    state = {
        showToast: false,
        isLoading: false,
        isDirty: false,
        searchActive: false,
        searchText: '',
        userMenuOpen: false,
        showMobileNavigation: false,
        modalActive: false,
        nameFieldValue: this.defaultState.nameFieldValue,
        emailFieldValue: this.defaultState.emailFieldValue,
        storeName: this.defaultState.nameFieldValue,
        supportSubject: '',
        supportMessage: '',
    };

    render() {
        const {
            showToast,
            isLoading,
            isDirty,
            searchActive,
            searchText,
            userMenuOpen,
            showMobileNavigation,
            nameFieldValue,
            emailFieldValue,
            modalActive,
            storeName,
        } = this.state;

        const toastMarkup = showToast ? (
            <Toast
                onDismiss={this.toggleState('showToast')}
                content="Changes saved"
            />
        ) : null;

        const userMenuActions = [
            {
                items: [{content: 'Community forums'}],
            },
        ];

        const navigationUserMenuMarkup = (
            <Navigation.UserMenu
                actions={userMenuActions}
                name="Dharma"
                detail={storeName}
                avatarInitials="D"
            />
        );

        const contextualSaveBarMarkup = isDirty ? (
            <ContextualSaveBar
                message="Unsaved changes"
                saveAction={{
                    onAction: this.handleSave,
                }}
                discardAction={{
                    onAction: this.handleDiscard,
                }}
            />
        ) : null;

        const userMenuMarkup = (
            <TopBar.UserMenu
                actions={userMenuActions}
                name="Dharma"
                detail={storeName}
                initials="D"
                open={userMenuOpen}
                onToggle={this.toggleState('userMenuOpen')}
            />
        );

        const searchResultsMarkup = (
            <Card>
                <ActionList
                    items={[
                        {content: 'Shopify help center'},
                        {content: 'Community forums'},
                    ]}
                />
            </Card>
        );

        const searchFieldMarkup = (
            <TopBar.SearchField
                onChange={this.handleSearchFieldChange}
                value={searchText}
                placeholder="Search"
            />
        );

        const topBarMarkup = (
            <TopBar
                showNavigationToggle={true}
                userMenu={userMenuMarkup}
                searchResultsVisible={searchActive}
                searchField={searchFieldMarkup}
                searchResults={searchResultsMarkup}
                onSearchResultsDismiss={this.handleSearchResultsDismiss}
                onNavigationToggle={this.toggleState('showMobileNavigation')}
            />
        );

        const navigationMarkup = (
            <Navigation location="/">
                <Navigation.Section
                    title={translations.customer_home.nav_1}
                    items={[


                        {
                            url: '/pickuprequestdetails',
                            label: translations.customer_home.nav_1_2,
                            icon: 'home',

                        },
                        {
                            url: '/pickuprequestshistory',
                            label: translations.customer_home.nav_1_3,
                            icon: 'home',

                        },
                    ]}
                />


                <Navigation.Section
                    title={translations.customer_home.nav_2}
                    items={[
                        {
                            url: '/shipmentlist',
                            label: translations.customer_home.nav_2_1,
                            icon: 'home',

                        },
                    ]}
                />

                <Navigation.Section
                    title={translations.customer_home.nav_3}
                    items={[
                        {
                            url: '/mailbox',
                            label: translations.customer_home.nav_3,
                            icon: 'home',

                        },
                    ]}
                />
                <Navigation.Section
                    title={translations.customer_home.nav_4}
                    items={[
                        {
                            url: '/addressbook',
                            label: translations.customer_home.nav_4,
                            icon: 'home',

                        },
                        {
                            url: '/editaddress',
                            label: translations.customer_home.nav_4_2,
                            icon: 'home',

                        },
                    ]}
                />
                <Navigation.Section
                    title={translations.customer_home.nav_5}
                    items={[
                        {
                            url: '/',
                            label: translations.customer_home.nav_5,
                            icon: 'home',

                        },
                    ]}
                />
                <Navigation.Section
                    title={translations.customer_home.nav_6}
                    items={[
                        {
                            url: '/',
                            label: translations.customer_home.nav_6,
                            icon: 'home',

                        },
                    ]}
                />
                <Navigation.Section
                    title={translations.customer_home.nav_7}
                    items={[
                        {
                            url: 'http://localhost:3000/',
                            label: translations.customer_home.nav_7,
                            icon: 'home',

                        },
                    ]}
                />
            </Navigation>

        );


        const loadingMarkup = isLoading ? <Loading /> : null;

        const actualPageMarkup = (
            <Page title={translations.customer_home.page_title}>
                <Layout>
                    <Layout.Section secondary>


                    </Layout.Section>
                </Layout>
            </Page>
        );

        const loadingPageMarkup = (
            <SkeletonPage>
                <Layout>
                    <Layout.Section>
                        <Card sectioned>
                            <TextContainer>
                                <SkeletonDisplayText size="small" />
                                <SkeletonBodyText lines={9} />
                            </TextContainer>
                        </Card>
                    </Layout.Section>
                </Layout>
            </SkeletonPage>
        );

        const pageMarkup = isLoading ? loadingPageMarkup : actualPageMarkup;

        const modalMarkup = (
            <Modal
                open={modalActive}
                onClose={this.toggleState('modalActive')}
                title="Contact support"
                primaryAction={{
                    content: 'Send',
                    onAction: this.toggleState('modalActive'),
                }}
            >
                <Modal.Section>
                    <FormLayout>
                        <TextField
                            label="Subject"
                            value={this.state.supportSubject}
                            onChange={this.handleSubjectChange}
                        />
                        <TextField
                            label="Message"
                            value={this.state.supportMessage}
                            onChange={this.handleMessageChange}
                            multiline
                        />
                    </FormLayout>
                </Modal.Section>
            </Modal>
        );

        const theme = {
            colors: {
                topBar: {
                    background: '#357997',
                },
            },

            logo: {
                width: 124,
                label : 'VIP Express',

            },
        };

        return (
            <div style={{height: '500px'}}>
                <AppProvider theme={theme}>
                    <Frame
                        topBar={topBarMarkup}
                        navigation={navigationMarkup}
                        showMobileNavigation={showMobileNavigation}
                        onNavigationDismiss={this.toggleState('showMobileNavigation')}
                    >
                        {contextualSaveBarMarkup}
                        {loadingMarkup}
                        {pageMarkup}
                        {toastMarkup}
                        {modalMarkup}
                    </Frame>
                </AppProvider>
            </div>
        );
    }

    toggleState = (key) => {
        return () => {
            this.setState((prevState) => ({[key]: !prevState[key]}));
        };
    };

    handleSearchFieldChange = (value) => {
        this.setState({searchText: value});
        if (value.length > 0) {
            this.setState({searchActive: true});
        } else {
            this.setState({searchActive: false});
        }
    };

    handleSearchResultsDismiss = () => {
        this.setState(() => {
            return {
                searchActive: false,
                searchText: '',
            };
        });
    };

    handleEmailFieldChange = (emailFieldValue) => {
        this.setState({emailFieldValue});
        if (emailFieldValue != '') {
            this.setState({isDirty: true});
        }
    };

    handleNameFieldChange = (nameFieldValue) => {
        this.setState({nameFieldValue});
        if (nameFieldValue != '') {
            this.setState({isDirty: true});
        }
    };

    handleSave = () => {
        this.defaultState.nameFieldValue = this.state.nameFieldValue;
        this.defaultState.emailFieldValue = this.state.emailFieldValue;

        this.setState({
            isDirty: false,
            showToast: true,
            storeName: this.defaultState.nameFieldValue,
        });
    };

    handleDiscard = () => {
        this.setState({
            emailFieldValue: this.defaultState.emailFieldValue,
            nameFieldValue: this.defaultState.nameFieldValue,
            isDirty: false,
        });
    };

    handleSubjectChange = (supportSubject) => {
        this.setState({supportSubject});
    };

    handleMessageChange = (supportMessage) => {
        this.setState({supportMessage});
    };
}


export default CustomerPickupRequestCreate;
