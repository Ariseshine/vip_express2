import React, { Component } from 'react';
import {Page, Layout, Card, FormLayout, TextField, Modal, TextContainer, InlineError} from '@shopify/polaris';
import ReactTable from "react-table";
import matchSorter from 'match-sorter';
import { connect } from 'react-redux';
import {
    loadProducts, loadConfigForOrder, addItem, removeItem, updateQuantity, closeModal, updateBasicInfo, updateShippingInfo, goToConfirmPage
} from './../redux/actions/actions';
import 'react-table/react-table.css';

import translationsJSON from '../translations/translations.json';
import LocalizedStrings from 'react-localization';
let translations = new LocalizedStrings(translationsJSON);




const mapStateToProps = state => {
    return {
        products: state.products.products,
        order: state.order
    }
}

class CustomerShipmentLists extends Component {
    constructor() {
        super();
        this.renderEditable = this.renderEditable.bind(this);
    }

    componentWillMount() {
        this.props.loadProducts();
        this.props.loadConfigForOrder();
    }

    renderEditable(cellInfo) {
        return (
            <div
                // style={{ backgroundColor: "#bdcabe" }}
                style={{ border: "1px solid grey" }}
                contentEditable
                suppressContentEditableWarning
                onBlur={ (e) => this.props.updateQuantity({index: cellInfo.index, quantity: e.target.innerHTML})}
                dangerouslySetInnerHTML={{
                    __html: this.props.order.items[cellInfo.index][cellInfo.column.id]
                }}
            />
        );
    }

    render() {
        return (
            <Page
                title={translations.order_page.title}
                pagination={{
                    hasPrevious: true,
                    previousURL: "/introduce",
                    hasNext: true,
                    // nextURL: "/confirm",
                    onNext: () => { this.props.goToConfirmPage(this.props.order) }
                }}
            >
                <Layout>
                    <Layout.Section>
                        <Card title={translations.order_page.section_1_title}>
                            <Card.Section>
                                <ReactTable
                                    data={this.props.products}
                                    filterable
                                    columns={[{
                                        Header: translations.order_page.table_column.category,
                                        accessor: 'category',
                                        filterMethod: (filter, rows) =>
                                            matchSorter(rows, filter.value, { keys: ["category"] }),
                                        filterAll: true
                                    },
                                        {
                                            Header: translations.order_page.table_column.brand,
                                            accessor: 'brand',
                                            filterMethod: (filter, rows) =>
                                                matchSorter(rows, filter.value, { keys: ["brand"] }),
                                            filterAll: true
                                        },
                                        {
                                            Header: translations.order_page.table_column.model,
                                            accessor: 'model',
                                            filterMethod: (filter, rows) =>
                                                matchSorter(rows, filter.value, { keys: ["model"] }),
                                            filterAll: true
                                        },
                                        {
                                            Header: translations.order_page.table_column.color,
                                            accessor: 'color',
                                            filterMethod: (filter, rows) =>
                                                matchSorter(rows, filter.value, { keys: ["color"] }),
                                            filterAll: true
                                        },
                                        {
                                            header: '',
                                            id: 'click-me-button',
                                            Cell: ({ row }) => (<button className="Polaris-Button Polaris-Button--primary" onClick={(e) => this.props.addItem(row._original, this.props.order.items)}>{ translations.button_add }</button>)
                                        }]}
                                    defaultPageSize={5}
                                    previousText={translations.table.previousText}
                                    nextText={translations.table.nextText}
                                    loadingText={translations.table.loadingText}
                                    noDataText={translations.table.noDataText}
                                    pageText={translations.table.pageText}
                                    ofText={translations.table.ofText}
                                    rowsText={translations.table.rowsText}
                                />
                            </Card.Section>
                        </Card>
                        <Card title={translations.order_page.section_2_title}>
                            <Card.Section>
                                <ReactTable
                                    data={this.props.order.items}
                                    columns={[{
                                        Header: translations.order_page.table_column.category,
                                        accessor: 'category'
                                    },
                                        {
                                            Header: translations.order_page.table_column.brand,
                                            accessor: 'brand'
                                        },
                                        {
                                            Header: translations.order_page.table_column.model,
                                            accessor: 'model'
                                        },
                                        {
                                            Header: translations.order_page.table_column.color,
                                            accessor: 'color'
                                        },
                                        {
                                            Header: translations.order_page.table_column.price,
                                            accessor: 'priceText'
                                        },
                                        {
                                            Header: translations.order_page.table_column.quantity,
                                            accessor: 'quantity',
                                            Cell: this.renderEditable
                                        },
                                        {
                                            Header: translations.order_page.table_column.amount,
                                            accessor: 'amountText'
                                        },
                                        {
                                            header: '',
                                            id: 'click-me-button',
                                            Cell: ({ row }) => (<button className="Polaris-Button Polaris-Button--primary" onClick={(e) => this.props.removeItem(row._index)}>{ translations.button_delete }</button>)
                                        }]}
                                    defaultPageSize={5}
                                    previousText={translations.table.previousText}
                                    nextText={translations.table.nextText}
                                    loadingText={translations.table.loadingText}
                                    noDataText={translations.table.noDataText}
                                    pageText={translations.table.pageText}
                                    ofText={translations.table.ofText}
                                    rowsText={translations.table.rowsText}
                                />
                                <FormLayout>
                                    <TextField readOnly label={translations.order_page.section_2_shipping_fee} value={this.props.order.shippingFeeText} onChange={() => {}} />
                                    <TextField readOnly label={translations.order_page.section_2_order_total_amount} value={this.props.order.totalAmountText} onChange={() => {}} />
                                </FormLayout>
                            </Card.Section>
                        </Card>
                        <Card title={translations.order_page.section_3_title}>
                            <Card.Section>
                                <FormLayout>
                                    <TextField type = 'email' label={translations.order_page.section_3_email} value={this.props.order.basicInfo.email} onChange={(val) => {this.props.updateBasicInfo({key: "email", value: val})}} />
                                    <InlineError message={translations.order_page.section_3_email + translations.errorMessage.inputRequired} fieldID="" />
                                    <TextField label={translations.order_page.section_3_phone} value={this.props.order.basicInfo.phone} onChange={(val) => {this.props.updateBasicInfo({key: "phone", value: val})}} />
                                    <TextField label="CPF/CNPJ" value={this.props.order.basicInfo.cpf_cnpj} onChange={(val) => {this.props.updateBasicInfo({key: "cpf_cnpj", value: val})}} />
                                </FormLayout>
                            </Card.Section>
                        </Card>
                        <Card title={translations.order_page.section_4_title} primaryFooterAction={{content: translations.button_continue, onAction: ()=> {this.props.goToConfirmPage(this.props.order)}}}>
                            <Card.Section>
                                <FormLayout>
                                    <TextField label={translations.order_page.section_4_name} value={this.props.order.shippingInfo.name} onChange={(val) => {this.props.updateShippingInfo({key: "name", value: val})}} />
                                    <InlineError message={translations.order_page.section_4_name + translations.errorMessage.inputRequired} fieldID="" />
                                    <TextField label={translations.order_page.section_4_address} value={this.props.order.shippingInfo.address} onChange={(val) => {this.props.updateShippingInfo({key: "address", value: val})}} />
                                    <InlineError message={translations.order_page.section_4_address + translations.errorMessage.inputRequired} fieldID="" />
                                    <TextField label={translations.order_page.section_4_city} value={this.props.order.shippingInfo.city} onChange={(val) => {this.props.updateShippingInfo({key: "city", value: val})}} />
                                    <InlineError message={translations.order_page.section_4_city + translations.errorMessage.inputRequired} fieldID="" />
                                    <TextField label="UF" value={this.props.order.shippingInfo.uf} onChange={(val) => {this.props.updateShippingInfo({key: "uf", value: val})}} />
                                    <TextField label="CEP" value={this.props.order.shippingInfo.cep} onChange={(val) => {this.props.updateShippingInfo({key: "cep", value: val})}} />
                                </FormLayout>
                            </Card.Section>
                        </Card>
                    </Layout.Section>
                </Layout>
                <div>
                    <Modal
                        open={this.props.order.inputError}
                        onClose={this.props.closeModal}
                    >
                        <Modal.Section>
                            <TextContainer>
                                <p>
                                    {this.props.order.inputErrorMessage}
                                </p>
                            </TextContainer>
                        </Modal.Section>
                    </Modal>
                </div>
            </Page>


        );
    }
}

export default connect(mapStateToProps, { loadProducts, loadConfigForOrder, addItem, removeItem, updateQuantity, closeModal, updateBasicInfo, updateShippingInfo, goToConfirmPage })(CustomerShipmentLists);
