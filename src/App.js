import React, { Component } from 'react';
import './App.css';

import CustomerHome from './components/CustomerHome';
import CustomerPickupRequestCreate from './components/CustomerPickupRequestCreate';
import CustomerPickupRequestsHistory from './components/CustomerPickupRequestsHistory';
import CustomerPickupRequestDetails from './components/CustomerPickupRequestDetails';
import CustomerShipmentList from './components/CustomerShipmentList';
import CustomerShipmentLists from './components/CustomerShipmentLists';
import MailBox from './components/MailBox'

import {AppProvider} from '@shopify/polaris';
import { Switch, Route } from 'react-router-dom';

import { connect } from 'react-redux';
import {
    checkSignIn
} from './redux/actions/actions';
import AddressBook from "./components/AddressBook";
import EditAddress from "./components/EditAddress";

class App extends Component {

    componentWillMount() {
        const pathname = window.location.pathname;
        if (pathname.includes('admin')){
            this.props.checkSignIn();
        }
    }

  render() {
    return (
        <AppProvider >
            <Switch>
                <Route exact path="/" component={CustomerHome} />
                <Route path="/pickuprequestcreate" component={CustomerPickupRequestCreate} />

                <Route path="/pickuprequestshistory" component={CustomerPickupRequestsHistory} />
                <Route exact path="/pickuprequestdetails" component={CustomerPickupRequestDetails} />

                <Route path="/shipmentlist" component={CustomerShipmentList} />
                <Route path="/mailbox" component={MailBox} />
                <Route path="/addressbook" component={AddressBook} />
                <Route path="/editaddress" component={EditAddress} />

                <Route path="/shipmentlists" component={CustomerShipmentLists} />

            </Switch>
        </AppProvider>
    );
  }
}

const mapStateToProps = state => {
    return {
    }
}

export default connect(mapStateToProps,{checkSignIn}) (App);
