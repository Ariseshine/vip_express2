import axios from 'axios'
import { push } from 'connected-react-router';
import currencyFormatter from 'currency-formatter';

import translationsJSON from '../../translations/translations.json';
import LocalizedStrings from 'react-localization';
let translations = new LocalizedStrings(translationsJSON);

const url = process.env.NODE_ENV === 'production' ? "/api/" : "http://localhost:5000/"
// const url = process.env.NODE_ENV === 'production' ? "/api/" : "https://5eba894c.ngrok.io/"

export function loadProducts () {
    return (dispatch) => {
        axios.get(`${url}products/list`)
            .then((res) => {
                let products = res.data.products
                dispatch({type:'LOAD_PRODUCTS', products})
            }).catch((err) => {
        })

        // let products = [{
        //     "id": "001",
        //     "category": "phone case",
        //     "brand": "samsung",
        //     "model": "s7",
        //     "color": "red"
        // },{
        //     "id": "002",
        //     "category": "phone case",
        //     "brand": "samsung",
        //     "model": "s8",
        //     "color": "red"
        // }];
        // dispatch({type:'LOAD_PRODUCTS', products})
    }
}

export function loadConfig () {
    return (dispatch) => {
        axios.get(`${url}configs`)
            .then((res) => {
                let configs = res.data;
                if (configs.length > 0){
                    let config = configs[0];
                    dispatch({type:'LOAD_CONFIG', config})
                }
            }).catch((err) => {
        })
    }
}

export function loadConfigForOrder () {
    return (dispatch) => {
        axios.get(`${url}configs`)
            .then((res) => {
                let configs = res.data;
                if (configs.length > 0){
                    let config = configs[0];
                    dispatch({type:'LOAD_CONFIG_FOR_ORDER', config})
                }
            }).catch((err) => {
        })
    }
}

export function loadProductsAdmin () {
    return (dispatch) => {
        axios.get(`${url}products?access_token=`+getTokenFormLocal())
            .then((res) => {
                let products = res.data;
                for(let i=0; i<products.length; i++){
                    if (products[i].active === true){
                        products[i]["active_text"] = translations.yes;
                    } else {
                        products[i]["active_text"] = translations.no;
                    }
                    products[i]["createAt"] = products[i]["createAt"].slice(0,19).replace("T", " ");
                    products[i]["changeAt"] = products[i]["changeAt"].slice(0,19).replace("T", " ");
                }
                dispatch({type:'LOAD_PRODUCTS', products})
            }).catch((err) => {
        })
    }
}

export function addItem (product, selectedItem) {
    return (dispatch) => {
        let has = false;
        for(let i=0; i<selectedItem.length; i++){
            if (selectedItem[i].id === product.id){
                has = true;
            }
        }

        if (has){
            let errorData = {
                inputError: true,
                inputErrorMessage: translations.errorMessage.itemSelected
            };
            dispatch({type:'SHOW_ERROR', errorData});
        } else {
            //get price
            axios.get(`${url}products/${product.id}`)
                .then((res) => {
                    let product_temp = res.data;
                    let item = Object.assign({}, product);
                    item["price"] = product_temp.price;
                    item["priceText"] = product_temp.priceText;
                    item["quantity"] = 1;
                    item["inventory"] = product_temp.quantity;
                    item["amount"] = item["price"] * item["quantity"];
                    item["amountText"] = currencyFormatter.format(item["amount"], { code: 'BRL' });

                    dispatch({type:'ADD_ITEM', item});
                }).catch((err) => {
            })
        }
    }
}

export function addProduct (product) {
    return (dispatch) => {
        axios.post(`${url}products?access_token=`+getTokenFormLocal(), product, )
            .then((res) => {
                this.loadProductsAdmin();
            }).catch((err) => {
                let errorData = {
                    inputError: true,
                    inputErrorMessage: translations.errorMessage.saveProductFailed
                };
                dispatch({type:'SHOW_ERROR', errorData});
        });
    }
}

export function editProduct (product) {
    return (dispatch) => {
        dispatch({type:'EDIT_PRODUCT', product})
    }
}

export function editOrder (order) {
    return (dispatch) => {
        dispatch({type:'EDIT_ORDER', order})
    }
}


export function editUser (user) {
    return (dispatch) => {
        dispatch({type:'EDIT_USER', user})
    }
}

export function saveProduct (product) {
    return (dispatch) => {
        product.changeAt = new Date().toISOString();
        delete product.createAt;
        axios.patch(`${url}products/${product.id}?access_token=`+getTokenFormLocal(), product)
            .then((res) => {
                dispatch(push('/admin/products'));
            }).catch((err) => {
            let errorData = {
                inputError: true,
                inputErrorMessage: translations.errorMessage.saveProductFailed
            };
            dispatch({type:'SHOW_ERROR', errorData});
        });
    }
}

export function saveUser (user) {
    return (dispatch) => {
        user.changeAt = new Date().toISOString();
        delete user.createAt;
        axios.patch(`${url}HonyanUsers/${user.id}?access_token=`+getTokenFormLocal(), user)
            .then((res) => {
                dispatch(push('/admin/users'));
            }).catch((err) => {
            let errorData = {
                inputError: true,
                inputErrorMessage: translations.errorMessage.saveUserFailed
            };
            dispatch({type:'SHOW_ERROR', errorData});
        })
    }
}

export function saveConfig (config) {
    return (dispatch) => {

        if (config.id){
            //update
            config.changeAt = new Date().toISOString();
            axios.patch(`${url}configs/${config.id}?access_token=`+getTokenFormLocal(), config)
                .then((res) => {
                    let errorData = {
                        inputError: true,
                        inputErrorMessage: translations.errorMessage.saveSuccess
                    };
                    dispatch({type:'SHOW_ERROR', errorData});
                }).catch((err) => {
                let errorData = {
                    inputError: true,
                    inputErrorMessage: translations.errorMessage.saveConfigFailed
                };
                dispatch({type:'SHOW_ERROR', errorData});
            })
        } else {
            //create
            config.createAt = new Date().toISOString();
            delete config.id;
            axios.post(`${url}configs?access_token=`+getTokenFormLocal(), config)
                .then((res) => {
                    let errorData = {
                        inputError: true,
                        inputErrorMessage: translations.errorMessage.saveSuccess
                    };
                    dispatch({type:'SHOW_ERROR', errorData});
                }).catch((err) => {
                let errorData = {
                    inputError: true,
                    inputErrorMessage: translations.errorMessage.saveConfigFailed
                };
                dispatch({type:'SHOW_ERROR', errorData});
            })
        }

    }
}


export function deleteProduct (product) {
    return (dispatch) => {
        axios.delete(`${url}products/${product.id}?access_token=`+getTokenFormLocal())
            .then((res) => {
                dispatch(push('/admin/products'));
            }).catch((err) => {
            let errorData = {
                inputError: true,
                inputErrorMessage: translations.errorMessage.saveProductFailed
            };
            dispatch({type:'SHOW_ERROR', errorData});
        });
    }
}

export function deleteUser (user) {
    return (dispatch) => {
        axios.delete(`${url}HonyanUsers/${user.id}?access_token=`+getTokenFormLocal())
            .then((res) => {
                dispatch(push('/admin/users'));
            }).catch((err) => {
            let errorData = {
                inputError: true,
                inputErrorMessage: translations.errorMessage.saveUserFailed
            };
            dispatch({type:'SHOW_ERROR', errorData});
        });
    }
}

export function loadOrders () {
    return (dispatch) => {
        axios.get(`${url}orders?filter[order]=createAt%20DESC&access_token=`+getTokenFormLocal())
            .then((res) => {
                let orders = res.data;
                for(let i=0; i<orders.length; i++){
                    orders[i]["createAt"] = orders[i]["createAt"].slice(0,19).replace("T", " ");
                    orders[i]["changeAt"] = orders[i]["changeAt"].slice(0,19).replace("T", " ");
                    orders[i]["excelLink"] = `${url}orders/download/${orders[i].id}?&access_token=`+getTokenFormLocal();
                    orders[i].basicInfo.status_text = '';
                    if (orders[i].basicInfo.status.open === true){
                        orders[i].basicInfo.status_text = translations.orderStatus.open;
                    } else{
                        if (orders[i].basicInfo.status.paid === true){
                            orders[i].basicInfo.status_text = translations.orderStatus.paid;
                        }
                        if (orders[i].basicInfo.status.shipped === true){
                            orders[i].basicInfo.status_text = orders[i].basicInfo.status_text + ' ' + translations.orderStatus.shipped;
                        }
                        if (orders[i].basicInfo.status.canceled === true){
                            orders[i].basicInfo.status_text = orders[i].basicInfo.status_text + ' ' + translations.orderStatus.canceled;
                        }
                        orders[i].basicInfo.status_text =  orders[i].basicInfo.status_text.trim();

                    }
                }
                dispatch({type:'LOAD_ORDERS', orders})
            }).catch((err) => {
        })
    }
}

export function loadUsers () {
    return (dispatch) => {
        axios.get(`${url}HonyanUsers?access_token=`+getTokenFormLocal())
            .then((res) => {
                let users = res.data;
                for(let i=0; i<users.length; i++){
                    users[i]["createAt"] = users[i]["createAt"].slice(0,19).replace("T", " ");
                    users[i]["changeAt"] = users[i]["changeAt"].slice(0,19).replace("T", " ");
                    if (users[i].active === true){
                        users[i]["active_text"] = translations.yes;
                    } else {
                        users[i]["active_text"] = translations.no;
                    }
                }
                dispatch({type:'LOAD_USERS', users})
            }).catch((err) => {
        })
    }
}

export function addUser (user) {
    return (dispatch) => {
        let errorData = {
            inputError: false,
            inputErrorMessage: '',
        };

        if (!user.name){
            errorData.inputError = true;
            errorData.inputErrorMessage = translations.errorMessage.pleaseTypeRequired;
        }
        if (!user.password){
            errorData.inputError = true;
            errorData.inputErrorMessage = translations.errorMessage.pleaseTypeRequired;
        }


        if(!validateEmail(user.email)){
            errorData.inputError = true;
            errorData.inputErrorMessage = translations.errorMessage.emailFormatError;
        }

        if (!user.email){
            errorData.inputError = true;
            errorData.inputErrorMessage = translations.errorMessage.pleaseTypeRequired;
        }

        validateEmailExist(user.email, dispatch);

        if(errorData.inputError === true){
            dispatch({type:'SHOW_ERROR', errorData});
        } else {
            axios.post(`${url}HonyanUsers?access_token=`+getTokenFormLocal(), user)
                .then((res) => {
                    this.loadUsers();
                }).catch((err) => {
                let errorData = {
                    inputError: true,
                    inputErrorMessage: translations.errorMessage.saveUserFailed
                };
                dispatch({type:'SHOW_ERROR', errorData});
            });
        }
    }


}

export function removeItem (index) {
    return (dispatch) => {
        dispatch({type:'REMOVE_ITEM', index})
    }
}

export function updateQuantity (data) {
    return (dispatch) => {
        dispatch({type:'UPDATE_QUANTITY', data})
    }
}

export function closeModal () {
    return (dispatch) => {
        let inputError = false;
        dispatch({type:'CLOSE_MODAL', inputError})
    }
}

export function updateBasicInfo (data) {
    return (dispatch) => {
        dispatch({type:'UPDATE_BASIC_INFO', data})
    }
}

export function updateCurrentProductState (key_value, add_product) {
    return (dispatch) => {
        let current_product = Object.assign({}, add_product);
        if (key_value.key === 'category'){
            current_product.category = key_value.value;
        } else if (key_value.key === 'brand'){
            current_product.brand = key_value.value;
        } else if (key_value.key === 'model'){
            current_product.model = key_value.value;
        } else if (key_value.key === 'color'){
            current_product.color = key_value.value;
        } else if (key_value.key === 'price'){
            current_product.priceText = key_value.value;
            current_product.price = currencyFormatter.unformat(key_value.value, { code: 'BRL' });
            current_product.priceText = currencyFormatter.format(current_product.price, { code: 'BRL' });
            current_product.price = currencyFormatter.unformat(current_product.priceText, { code: 'BRL' });
        } else if (key_value.key === 'quantity'){
            current_product.quantity = Number(key_value.value);
        } else if (key_value.key === 'active'){
            current_product.active = key_value.value;
        }
        dispatch({type:'UPDATE_CURRENT_PRODUCT_STATE', current_product})
    }
}

export function updateCurrentUserState (key_value, user) {
    return (dispatch) => {
        let currentUser = Object.assign({}, user);
        if (key_value.key === 'name'){
            currentUser.name = key_value.value;
        } else if (key_value.key === 'email'){
            currentUser.email = key_value.value;
        } else if (key_value.key === 'password'){
            currentUser.password = key_value.value;
        } else if (key_value.key === 'active'){
            currentUser.active = key_value.value;
        }
        dispatch({type:'UPDATE_CURRENT_USER_STATE', currentUser})
    }
}

export function signIn (user) {
    return (dispatch) => {
        //get user id
        axios.post(`${url}HonyanUsers/login`, {
                email: user.email,
                password: user.password,
                ttl: 86400
            })
            .then((res) => {
                //get user
                let token = res.data.id;
                let userId = res.data.userId
                axios.get(`${url}HonyanUsers/${userId}?access_token=${token}`)
                    .then((res) => {
                        let userDb = res.data;
                        userDb.password = "";
                        if (userDb.name){
                            let firstLetter = userDb.name.charAt(0);
                            firstLetter = firstLetter.toUpperCase();
                            userDb['firstLetter'] = firstLetter;

                        }
                        userDb['token'] = token;
                        localStorage.setItem('Auth', JSON.stringify(userDb));
                        dispatch({type: 'SET_USER', userDb});
                        dispatch(push('/admin'));
                    }).catch((err) => {
                    let errorData = {
                        inputError: true,
                        inputErrorMessage: translations.errorMessage.signInFailed
                    };
                    dispatch({type:'SHOW_ERROR', errorData});
                })
            }).catch((err) => {
            let errorData = {
                inputError: true,
                inputErrorMessage: translations.errorMessage.signInFailed
            };
            dispatch({type:'SHOW_ERROR', errorData});
        });
    }
}

export function checkSignIn () {
    return (dispatch) => {
        if(localStorage.Auth){
            let userDb = JSON.parse(localStorage.Auth);
            axios.get(`${url}HonyanUsers/${userDb.id}/accessTokens?access_token=${userDb.token}`)
                .then((res) => {
                    dispatch({type:'SET_USER', userDb});
                }).catch((err) => {
                dispatch(push('/admin/signin'));
            });
        } else {
            dispatch(push('/admin/signin'));
        }
    }
}

export function logOut () {
    return (dispatch) => {
        axios.post(`${url}HonyanUsers/logout?access_token=`+getTokenFormLocal())
            .then((res) => {
                localStorage.removeItem('Auth');
                let userDb = {};
                dispatch({type:'SET_USER', userDb});
                dispatch(push('/admin/signin'));

            }).catch((err) => {
            localStorage.removeItem('Auth');
            let userDb = {};
            dispatch({type:'SET_USER', userDb});
            dispatch(push('/admin/signin'));
        });
    }
}


export function updateCurrentConfigState (config) {
    return (dispatch) => {
        dispatch({type:'UPDATE_CURRENT_CONFIG_STATE', config})
    }
}

export function updateShippingInfo (data) {
    return (dispatch) => {
        dispatch({type:'UPDATE_SHIPPING_INFO', data})
    }
}


export function goToConfirmPage (order) {
    return (dispatch) => {
        let errorData = {
            inputError: false,
            inputErrorMessage: '',
        };

        if (!order.shippingInfo.name){
            errorData.inputError = true;
            errorData.inputErrorMessage = translations.errorMessage.pleaseTypeRequired;
        }
        if (!order.shippingInfo.address){
            errorData.inputError = true;
            errorData.inputErrorMessage = translations.errorMessage.pleaseTypeRequired;
        }
        if (!order.shippingInfo.city){
            errorData.inputError = true;
            errorData.inputErrorMessage = translations.errorMessage.pleaseTypeRequired;
        }
        if(!validateEmail(order.basicInfo.email)){
            errorData.inputError = true;
            errorData.inputErrorMessage = translations.errorMessage.emailFormatError;
        }
        if (!order.basicInfo.email){
            errorData.inputError = true;
            errorData.inputErrorMessage = translations.errorMessage.pleaseTypeRequired;
        }

        //check inventory
        for(let i=0; i<order.items.length; i++){
            if (order.items[i].quantity > order.items[i].inventory){
                errorData.inputError = true;
                errorData.inputErrorMessage = order.items[i].category + ' ' + order.items[i].brand + ' ' + order.items[i].model + ' ' + order.items[i].color + ' ' + translations.errorMessage.notEnoughInventory;
            }
        }

        if(errorData.inputError === true){
            dispatch({type:'SHOW_ERROR', errorData});
        } else {
            dispatch(push('/confirm'));
        }
    }
}

export function submitOrder (order) {
    return (dispatch) => {
        let order_db = {
            items: order.items,
            totalAmount: order.totalAmount,
            shippingFee: order.shippingFee,
            totalAmountText: order.totalAmountText,
            shippingFeeText: order.shippingFeeText,
            basicInfo: order.basicInfo,
            shippingInfo: order.shippingInfo
        }

        axios.post(`${url}orders`, order_db)
            .then((res) => {
                let submitData = {
                    orderNumber: res.data.id,
                    submitMessage: translations.status_page.success
                }
                dispatch({type:'SUBMIT_ORDER', submitData});
            }).catch((err) => {
                let submitData = {
                    orderNumber: '',
                    submitMessage: translations.status_page.failed
                }
                dispatch({type:'SUBMIT_ORDER', submitData});
        })

    }
}

export function submitEditOrder (order) {
    return (dispatch) => {
        let errorData = {
            inputError: false,
            inputErrorMessage: '',
        };

        if (!order.shippingInfo.name){
            errorData.inputError = true;
            errorData.inputErrorMessage = translations.errorMessage.pleaseTypeRequired;
        }
        if (!order.shippingInfo.address){
            errorData.inputError = true;
            errorData.inputErrorMessage = translations.errorMessage.pleaseTypeRequired;
        }
        if (!order.shippingInfo.city){
            errorData.inputError = true;
            errorData.inputErrorMessage = translations.errorMessage.pleaseTypeRequired;
        }
        if(!validateEmail(order.basicInfo.email)){
            errorData.inputError = true;
            errorData.inputErrorMessage = translations.errorMessage.emailFormatError;
        }
        if (!order.basicInfo.email){
            errorData.inputError = true;
            errorData.inputErrorMessage = translations.errorMessage.pleaseTypeRequired;
        }

        if(errorData.inputError === true){
            dispatch({type:'SHOW_ERROR', errorData});
        } else {
            let order_db = {
                items: order.items,
                totalAmount: order.totalAmount,
                shippingFee: order.shippingFee,
                basicInfo: order.basicInfo,
                shippingInfo: order.shippingInfo,
                totalAmountText: order.totalAmountText,
                shippingFeeText: order.shippingFeeText,
                status: order.status,
                changeAt: new Date().toISOString(),
            }

            axios.patch(`${url}orders/${order.orderNumber}?access_token=`+getTokenFormLocal(), order_db)
                .then((res) => {
                    dispatch(push('/admin/orders'));
                }).catch((err) => {
                errorData.inputError = true;
                errorData.inputErrorMessage = translations.status_page.failed
                dispatch({type:'SHOW_ERROR', errorData});
            });
        }


    }
}

function validateEmail(email) {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function validateEmailExist(email, dispatch) {
    axios.get(`${url}HonyanUsers?filter[where][email]=${email}&access_token=`+getTokenFormLocal())
        .then((res) => {
            let users = res.data;
            for(let i=0; i<users.length; i++){
                if (users[i].email === email){
                    let errorData = {
                        inputError: true,
                        inputErrorMessage: translations.errorMessage.emailIsExist
                    };
                    dispatch({type:'SHOW_ERROR', errorData});
                }
            }
        }).catch((err) => {
            let errorData = {
                inputError: true,
                inputErrorMessage: translations.errorMessage.saveUserFailed
            };
            dispatch({type:'SHOW_ERROR', errorData});
    })
}

// function getLogInAuth() {
//     let auth = {auth: {
//             username: "",
//             password: ""
//         }};
//     if (localStorage.Auth){
//         let userDb = JSON.parse(localStorage.Auth);
//         auth.auth.username = userDb.email;
//         auth.auth.password = userDb.password;
//     }
//
//     return auth;
// }
function getTokenFormLocal() {
    let token = '';

    if (localStorage.Auth){
        let userDb = JSON.parse(localStorage.Auth);
        token = userDb.token;
    }

    return token;
}


