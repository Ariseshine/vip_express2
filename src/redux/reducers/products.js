const initialState = {
    products: [],
    current_product: {active: true, priceText: "R$ 0,00"},
    hasError: false,
    errorMessage: ""
}
export default (state=initialState, action) => {
    switch (action.type) {
        case 'LOAD_PRODUCTS' :

            return {
                ...state,
                products: action.products,
                current_product: initialState.current_product

            }
        case 'UPDATE_CURRENT_PRODUCT_STATE' :
            let current_product_new = Object.assign({}, action.current_product);
            return {
                ...state,
                current_product: current_product_new
            }
        case 'EDIT_PRODUCT' :
            current_product_new = Object.assign({}, action.product);
            return {
                ...state,
                current_product: current_product_new
            }
        default:
            return state
    }
}