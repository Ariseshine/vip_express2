const initialState = {
    users: [],
    currentUser:{email:"",active: true},
    loginUser:{}
}
export default (state=initialState, action) => {
    switch (action.type) {
        case 'LOAD_USERS' :
            return {
                ...state,
                users: action.users,
                currentUser: initialState.currentUser
            }
        case 'EDIT_USER' :
            let user_new = Object.assign({}, action.user);
            return {
                ...state,
                currentUser: user_new
            }
        case 'SET_USER' :
            let loginUser = Object.assign({}, action.userDb);
            return {
                ...state,
                loginUser: loginUser
            }
        case 'GET_USERS' :
            return {
                ...state,
                users: action.users
            }
        case 'UPDATE_CURRENT_USER_STATE' :
            let currentUser_new = Object.assign({}, action.currentUser);
            return {
                ...state,
                currentUser: currentUser_new
            }
        default:
            return state
    }
}