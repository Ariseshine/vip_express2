const initialState = {
    config:{
        "id": "",
        "configInfo":{
            shippingFeeRule: {
                r1:{
                    fromAmount: 0,
                    toAmount: 500,
                    shippingFee: 50
                },
                r2:{
                    fromAmount: 501,
                    toAmount: 1000,
                    shippingFee: 100
                },
                r3:{
                    fromAmount: 1001,
                    toAmount: 2000,
                    shippingFee: 200
                },
                r4:{
                    fromAmount: 2001,
                    toAmount: 99999999,
                    shippingFee: 300
                }
            }
        }
    }
}
export default (state=initialState, action) => {
    switch (action.type) {
        case 'LOAD_CONFIG' :

            return {
                ...state,
                config: action.config

            }
        case 'UPDATE_CURRENT_CONFIG_STATE' :
            let config_new = Object.assign({}, action.config);
            return {
                ...state,
                config: config_new
            }
        default:
            return state
    }
}