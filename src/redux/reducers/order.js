import translationsJSON from '../../translations/translations.json';
import LocalizedStrings from 'react-localization';
import currencyFormatter from 'currency-formatter';
let translations = new LocalizedStrings(translationsJSON);

const initialState = {
    orderNumber: '',
    items: [],

    totalAmount: 0,
    totalAmountText: 0,
    shippingFee: 0,
    shippingFeeText: 0,

    basicInfo: {
        tracking_number: '',
        email: '',
        phone: '',
        cpf_cnpj: '',
        status: {
            open: true,
            canceled: false,
            paid: false,
            shipped: false
        }
    },
    shippingInfo: {
        name: '',
        address: '',
        city: '',
        uf: '',
        cep: ''
    },
    inputError: false,
    inputErrorMessage: '',
    returnMessage: '',

    orders: [],

    status_options: [
        {label: translations.orderStatus.open, value: 'open'},
        {label: translations.orderStatus.canceled, value: 'canceled'},
        {label: translations.orderStatus.paid, value: 'paid'},
        {label: translations.orderStatus.shipped, value: 'shipped'},
    ],

    config: {}
}
export default (state=initialState, action) => {
    switch (action.type) {
        case 'LOAD_ORDERS' :
            return {
                ...state,
                orders: action.orders,
                config: action.config
            }
        case 'LOAD_CONFIG_FOR_ORDER' :
            return {
                ...state,
                config: action.config
            }
        case 'ADD_ITEM' :
            let items = Object.assign([], state.items);
            items.push(action.item);
            let totalAmount = getTotalAmount(items);
            let shippingFee = getShippingFee(totalAmount, state.config);
            let totalAmountText = currencyFormatter.format(totalAmount, { code: 'BRL' });
            let shippingFeeText = currencyFormatter.format(shippingFee, { code: 'BRL' });
            return {
                ...state,
                items: items,
                totalAmount: totalAmount,
                totalAmountText: totalAmountText,
                shippingFeeText: shippingFeeText,
                shippingFee: shippingFee
            }
        case 'REMOVE_ITEM' :
            items = Object.assign([], state.items);
            items.splice(action.index,1);
            totalAmount = getTotalAmount(items);
            shippingFee = getShippingFee(totalAmount, state.config);
            totalAmountText = currencyFormatter.format(totalAmount, { code: 'BRL' });
            shippingFeeText = currencyFormatter.format(shippingFee, { code: 'BRL' });
            return {
                ...state,
                items: items,
                totalAmount: totalAmount,
                shippingFee: shippingFee,
                totalAmountText: totalAmountText,
                shippingFeeText: shippingFeeText,
            }
        case 'UPDATE_QUANTITY' :
            items = Object.assign([], state.items);

            let inputError = true;
            let inputErrorMessage = '';
            if (Number.isInteger(Number(action.data.quantity))===true){
                inputError = false;
                if (parseInt(action.data.quantity) > items[action.data.index]["inventory"]){
                    inputError = true;
                    inputErrorMessage = translations.errorMessage.notEnoughInventory;
                }
                items[action.data.index]["quantity"] =parseInt(action.data.quantity);

                let amount = parseFloat(items[action.data.index]["quantity"]) * parseFloat(items[action.data.index]["price"]);
                items[action.data.index]["amount"] = amount.toFixed(2);
                items[action.data.index]["amountText"] = currencyFormatter.format(items[action.data.index]["amount"], { code: 'BRL' });
            } else {
                inputError = true;
                inputErrorMessage = translations.errorMessage.needInteger;
            }

            totalAmount = getTotalAmount(items);
            shippingFee = getShippingFee(totalAmount, state.config);
            totalAmountText = currencyFormatter.format(totalAmount, { code: 'BRL' });
            shippingFeeText = currencyFormatter.format(shippingFee, { code: 'BRL' });
            return {
                ...state,
                items: items,
                inputError: inputError,
                totalAmount: totalAmount,
                shippingFee: shippingFee,
                inputErrorMessage: inputErrorMessage,
                totalAmountText: totalAmountText,
                shippingFeeText: shippingFeeText,
            }
        case 'UPDATE_BASIC_INFO' :
            let basicInfo = Object.assign({}, state.basicInfo);
            if (action.data.key==="email"){
                basicInfo.email = action.data.value;
            } else if (action.data.key==="phone") {
                basicInfo.phone = action.data.value;
            }else if (action.data.key==="tracking_number"){
                basicInfo.tracking_number = action.data.value;
            } else if (action.data.key==="cpf_cnpj"){
                basicInfo.cpf_cnpj = action.data.value;
            } else if (action.data.key==="status-paid"){
                basicInfo.status.paid = action.data.value;
            } else if (action.data.key==="status-shipped"){
                basicInfo.status.shipped = action.data.value;
            } else if (action.data.key==="status-canceled"){
                basicInfo.status.canceled = action.data.value;
            }

            if (basicInfo.status.canceled === false && basicInfo.status.shipped === false && basicInfo.status.paid === false){
                basicInfo.status.open = true;
            } else {
                basicInfo.status.open = false;
            }

            return {
                ...state,
                basicInfo: basicInfo
            }
        case 'UPDATE_SHIPPING_INFO' :
            let shippingInfo = Object.assign({}, state.shippingInfo);
            if (action.data.key==="name"){
                shippingInfo.name = action.data.value;
            } else if (action.data.key==="address"){
                shippingInfo.address = action.data.value;
            } else if (action.data.key==="city"){
                shippingInfo.city = action.data.value;
            } else if (action.data.key==="uf"){
                shippingInfo.uf = action.data.value;
            } else if (action.data.key==="cep"){
                shippingInfo.cep = action.data.value;
            }
            return {
                ...state,
                shippingInfo: shippingInfo
            }
        case 'CLOSE_MODAL' :
            return {
                ...state,
                inputError: action.inputError,
                inputErrorMessage: ''
            }
        case 'SHOW_ERROR' :
            return {
                ...state,
                inputError: action.errorData.inputError,
                inputErrorMessage: action.errorData.inputErrorMessage
            }
        case 'SUBMIT_ORDER' :
            return {
                ...state,
                orderNumber: action.submitData.orderNumber,
                submitMessage: action.submitData.submitMessage
            }
        case 'EDIT_ORDER' :
            return {
                ...state,
                orderNumber: action.order.id,
                items: action.order.items,

                totalAmount: action.order.totalAmount,
                shippingFee: action.order.shippingFee,
                totalAmountText: action.order.totalAmountText,
                shippingFeeText: action.order.shippingFeeText,

                basicInfo: action.order.basicInfo,
                shippingInfo: action.order.shippingInfo
            }
        default:
            return state
    }
}

function getTotalAmount(items){
    let total_amount = 0;
    for ( let i in items) {
        total_amount = parseFloat(total_amount) + parseFloat(items[i].amount);
    }
    total_amount = total_amount.toFixed(2);
    return Number(total_amount);
}

function getShippingFee(totalAmount, config){
    let shippingFee = 0;
    if (totalAmount >=config.configInfo.shippingFeeRule.r1.fromAmount && totalAmount <=config.configInfo.shippingFeeRule.r1.toAmount) {
        shippingFee = config.configInfo.shippingFeeRule.r1.shippingFee;
    }  else if (totalAmount >=config.configInfo.shippingFeeRule.r2.fromAmount && totalAmount <=config.configInfo.shippingFeeRule.r2.toAmount) {
        shippingFee = config.configInfo.shippingFeeRule.r2.shippingFee;
    } else if (totalAmount >=config.configInfo.shippingFeeRule.r3.fromAmount && totalAmount <=config.configInfo.shippingFeeRule.r3.toAmount) {
        shippingFee = config.configInfo.shippingFeeRule.r3.shippingFee;
    } else if (totalAmount >=config.configInfo.shippingFeeRule.r4.fromAmount && totalAmount <=config.configInfo.shippingFeeRule.r4.toAmount) {
        shippingFee = config.configInfo.shippingFeeRule.r4.shippingFee;
    } else {
        shippingFee = 50;
    }

    return shippingFee;
}