import { combineReducers } from 'redux';
import order from './reducers/order';
import products from './reducers/products';
import users from './reducers/users';
import config from './reducers/config';
import { connectRouter } from 'connected-react-router'

// export default combineReducers({
//     products,
//     order,
//     router: routerReducer
// });

export default (history) => combineReducers({
    products,
    order,
    users,
    config,
    router: connectRouter(history)
});
